
const up = function(knex) {
    return knex.schema.createTable('channels', function(table){
        table.increments('id').primary();
        table.string('channel', 50);
        table.string('channel_type', 100);
        table.string('icon', 50);
        table.text('description');
        table.boolean('active');
        table.string('created_by', 50);
        table.string('updated_by', 50);
        table.timestamps();
    })
};

const down = function(knex) {
    return knex.schema.dropTable('channels');
};

module.exports = {up, down}