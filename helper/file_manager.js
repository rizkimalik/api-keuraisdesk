"use strict";
const path = require('path');
const fs = require('fs');
const https = require('https');
const logger = require('./logger');


const UploadAttachment = async function (value) {
    const { channel, attachment, file_name, file_size } = value;
    try {
        const extension = path.extname(file_name);
        const directory = `./${process.env.DIR_ATTACHMENT}/${channel}/`;
        const allowedExtensions = /png|jpeg|jpg|gif|svg|pdf|xls|xlsx|doc/;

        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        if (!allowedExtensions.test(extension)) throw "Invalid File type";
        if (file_size > 3000000) throw "max file 3MB";
        // const md5 = file.md5;
        // const url = `${directory + md5 + extension}`;
        const url = `${directory + file_name}`;

        fs.writeFile(url, attachment, function (err) {
            if (err) {
                logger('attachment/UploadAttachment', err);
                return console.log(err);
            }
        });

        return url;
    } catch (error) {
        console.log(error)
        logger('attachment/UploadAttachment', error);
    }
}

const DownloadFromBase64 = async function (value) {
    const { channel, message_type, message_id, message_raw } = value;
    try {
        const directory = `./${process.env.DIR_ATTACHMENT}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }

        let dataBase64 = '';
        let fileName = '';
        if (message_type === 'image') {
            fileName = message_id + '.jpeg';
            dataBase64 = message_raw.imageMessage.jpegThumbnail;
        }
        const url = `${directory + fileName}`;

        fs.writeFile(url, dataBase64, 'base64', function (err) {
            if (err) {
                logger('attachment/DownloadAttachment', err);
                return console.log(err);
            }
        });

        return url;
    } catch (error) {
        console.log(error)
        logger('attachment/DownloadAttachment', error);
    }
}

const DownloadFromURL = async function (value) {
    const { channel, message_type, message_id, message_raw, attachment_url } = value;
    try {
        const directory = `./${process.env.DIR_ATTACHMENT}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }

        let filename = '';
        if (message_type === 'image') {
            filename = message_id + '.jpeg';
        }
        else if (message_type === 'document') {
            const extension = path.extname(message_raw.documentMessage.fileName);
            filename = message_id + extension;
        }
        const path_url = `${directory + filename}`;

        https.get(process.env.WA_API_URL + attachment_url, (res) => {
            const filePath = fs.createWriteStream(path_url);
            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
                console.log('Download Completed');
                return path_url.replace('./','');
            })
        })
        return path_url.replace('./','');
        
    } catch (error) {
        console.log(error)
        logger('attachment/DownloadAttachmentURL', error);
    }
}


module.exports = { 
    UploadAttachment, 
    DownloadFromBase64, 
    DownloadFromURL 
}