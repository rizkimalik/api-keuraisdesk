const APP_URL = process.env.APP_URL;
const WA_API_URL = process.env.WA_API_URL;
const WA_API_KEY = process.env.WA_API_KEY;

module.exports = {
    APP_URL,
    WA_API_URL,
    WA_API_KEY,
}