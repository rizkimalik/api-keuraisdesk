'use strict';
const path = require('path');
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { logger, random_string, response, file_manager } = require('../../helper');
const { UploadAttachment } = file_manager;

//? HTTP FUNCTION
const join_chat = async function (req, res) {
    try {
        const data = req.body;
        if (data.email && data.flag_to === 'customer') {
            const result = await customer_join(data);
            response.ok(res, result);
        }
        else if (data.username && data.flag_to === 'agent') {
            const result = await agent_join(data);
            response.ok(res, result);
        }
        else {
            response.error(res, 'error', 'chat/join_chat');
        }
    }
    catch (error) {
        console.log(error);
        logger('chat/join_chat', error);
    }
}

const list_customers = async function (req, res) {
    const { agent_handle } = req.body;
    const list_customers = await knex('v_omnichannel_customer').where({ agent_handle }).orderBy('connected', 'DESC');
    response.ok(res, list_customers);
}

const conversation_chats = async function (req, res) {
    try {
        const { chat_id, customer_id } = req.body;

        await knex('chats').update({ flag_notif: '1' }).where({ chat_id, customer_id }); //flag read notif
        const conversations = await knex('chats')
            .select('chat_id', 'user_id', 'customer_id', 'name', 'email', 'flag_to', 'message', 'message_type', 'message_id', 'date_create', 'channel', 'flag_notif')
            .where({ chat_id, customer_id, flag_end: 'N' })
            .orderBy('id', 'asc')

            for (let i = 0; i < conversations.length; i++) {
                conversations[i].date_create = date.format(conversations[i].date_create, 'YYYY-MM-DD HH:mm:ss', true)
                let attachment = await knex('chat_attachments').where({ chat_id: conversations[i].chat_id, message_id: conversations[i].message_id })
                conversations[i].attachment = attachment ? attachment : '';
            }
        response.ok(res, conversations);
    }
    catch (error) {
        console.log(error);
        logger('chat/conversation_chats', error);
    }
}

const end_chat = async function (req, res) {
    try {
        const { chat_id, customer_id } = req.body;
        const res_endchat = await knex.raw(`
            UPDATE chats SET flag_end='Y' WHERE chat_id='${chat_id}' AND customer_id='${customer_id}'
            -- INSERT INTO chats_end SELECT * FROM chats WHERE flag_end='Y'
            -- DELETE chats WHERE flag_end='Y'
        `);
        response.ok(res, res_endchat);
    }
    catch (error) {
        console.log(error);
        logger('chat/end_chat', error);
    }

}


//? NON HTTP FUNCTION
const customer_join = async function (data) {
    const now = new Date();
    const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const message_id = random_string(20);

    const customer = await knex('customers').select('customer_id').where({ email: data.email }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.username,
                email: data.email,
                uuid: data.uuid,
                connected: data.connected,
                source: 'Chat',
                status: 'Registered',
                created_at: knex.fn.now()
            }]);
    }
    else {
        await knex('customers').update({ uuid: data.uuid, connected: data.connected }).where({ email: data.email });
    }

    const chat = await knex('chats').select('chat_id')
        .where({
            email: data.email,
            flag_to: 'customer',
            status_chat: 'waiting',
            flag_end: 'N',
            channel: 'Chat',
        }).first();

    const chat_id = chat ? chat.chat_id : generate_chatid;
    if (!chat) {
        await knex('chats')
            .insert([{
                chat_id: chat_id,
                user_id: data.email,
                message: 'Joined Chat',
                message_type: 'text',
                message_id,
                name: data.username,
                email: data.email,
                channel: 'Chat',
                customer_id: customer_id,
                flag_to: 'customer',
                status_chat: 'waiting',
                flag_end: 'N',
                date_create: knex.fn.now()
            }]);
    }

    // get result data & send
    const result = await knex('chats')
        .where({
            email: data.email,
            flag_to: 'customer',
            // status_chat: 'open',
            flag_end: 'N',
            channel: 'Chat',
        }).first();

    if (result) {
        const user = await knex('users').select('uuid').where({ username: result.agent_handle }).first();
        const cust = await knex('customers').select('uuid').where({ email: data.email }).first();
        result.uuid_agent = user?.uuid;
        result.uuid_customer = cust?.uuid;
    }

    return result;
}

const agent_join = async function (data) {
    const user = await knex('users').select('username').where({ username: data.username }).first();

    if (user) {
        await knex('users')
            .update({ uuid: data.uuid, connected: data.connected, login: '1' })
            .where({ username: data.username, user_level: 'Layer1' });
    }

    return user;
}

const send_message_customer = async function (req) {
    try {
        const {
            chat_id,
            user_id,
            customer_id,
            name,
            email,
            message,
            message_type,
            agent_handle,
            attachment
        } = req;
        const channel = 'Chat';
        const message_id = random_string(20);


        await knex('chats')
            .insert([{
                chat_id,
                user_id,
                customer_id,
                name,
                email,
                message,
                message_type,
                message_id,
                agent_handle,
                channel,
                flag_to: 'customer',
                status_chat: 'open',
                flag_end: 'N',
                date_create: knex.fn.now()
            }]);

        //upload file attachment
        if (attachment) {
            for (let i = 0; i < attachment.length; i++) {
                let value = { chat_id, channel, message_id, message_type, attachment: attachment[i].attachment, file_origin: attachment[i].file_origin, file_name: attachment[i].file_name, file_size: attachment[i].file_size, file_url: attachment[i].file_url }
                await UploadAttachment(value);
                await insertDataAttachment(value);
            }
        }

    }
    catch (error) {
        console.log(error);
        logger('chat/send_message_customer', error);
    }
}

const send_message_agent = async function (req) {
    try {
        const {
            chat_id,
            user_id,
            customer_id,
            name,
            email,
            message,
            message_type,
            agent_handle,
            attachment
        } = req;
        const message_id = random_string(20);
        const channel = 'Chat';

        await knex('chats')
            .insert([{
                chat_id,
                user_id,
                customer_id,
                name,
                email,
                message,
                message_type,
                message_id,
                agent_handle,
                channel,
                flag_to: 'agent',
                status_chat: 'open',
                flag_end: 'N',
                date_create: knex.fn.now()
            }]);

        //upload file attachment
        if (attachment) {
            for (let i = 0; i < attachment.length; i++) {
                let value = { chat_id, channel, message_id, message_type, attachment: attachment[i].attachment, file_origin: attachment[i].file_origin, file_name: attachment[i].file_name, file_size: attachment[i].file_size, file_url: attachment[i].file_url }
                await UploadAttachment(value);
                await insertDataAttachment(value);
            }
        }

    }
    catch (error) {
        console.log(error);
        logger('chat/send_message_agent', error);
    }
}

const insertDataAttachment = async function (data) {
    try {
        await knex('chat_attachments')
            .insert([{
                chat_id: data.chat_id,
                message_id: data.message_id,
                file_origin: data.file_origin,
                file_name: data.file_name,
                file_type: data.message_type,
                file_url: data.file_url,
                file_size: data.file_size,
            }]);
    } catch (error) {
        logger('chat/insert_attachment', error);
    }
}

const update_socket = async function (data) {
    try {
        if (data.flag_to === 'customer') {
            await knex('customers')
                .update({ uuid: data.uuid, connected: data.connected })
                .where({ email: data.email });
        }
        else {
            await knex('users')
                .update({ uuid: data.uuid, connected: data.connected, login: data.login })
                .where({ username: data.username });
        }
    }
    catch (error) {
        console.log(error);
        logger('chat/update_socket', error);
    }
}

module.exports = {
    list_customers,
    join_chat,
    send_message_customer,
    send_message_agent,
    conversation_chats,
    end_chat,
    update_socket,
}
