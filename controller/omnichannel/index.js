const facebook = require('./facebook');
const twitter = require('./twitter');
const instagram = require('./instagram');
const chat = require('./chat');
const email = require('./email');
const whatsapp = require('./whatsapp');

module.exports =  {
    facebook,
    twitter,
    instagram,
    chat,
    email,
    whatsapp,
}