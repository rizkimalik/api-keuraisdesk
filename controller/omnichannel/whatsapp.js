const path = require('path');
const axios = require('axios');
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { APP_URL, WA_API_URL, WA_API_KEY } = require('../../config/config');
const { response, logger, file_manager, random_string } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');
const { UploadAttachment } = require('../../helper/file_manager');

const whatsapp_webhook = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const now = new Date();
        const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.sender_phone,
                flag_to: 'customer',
                flag_end: 'N',
                channel: 'Whatsapp',
            }).first();

        data.customer_id = await insertDataCustomer(data);;
        data.chat_id = chat ? chat.chat_id : generate_chatid;
        data.channel = 'Whatsapp';

        await knex('chats')
            .insert([{
                chat_id: data.chat_id,
                user_id: data.sender_phone,
                message: data.message_text,
                message_type: data.message_type,
                name: data.sender_push_name,
                email: data.sender_phone,
                // agent_handle: chat.agent_handle,
                message_id: data.message_id,
                channel: data.channel,
                customer_id: data.customer_id,
                flag_to: 'customer',
                status_chat: 'open',
                flag_end: 'N',
                // date_create: data.message_timestamp
                date_create: knex.fn.now()
            }]);

        if (data.message_type !== 'text') {
            const path_url = await file_manager.DownloadFromURL(data);
            data.url = path_url;
            await insertDataAttachment(data);
        }
        response.ok(res, data);
    }
    catch (error) {
        console.log(error);
        logger('omnichannel/whatsapp', error);
        res.status(500).end();
    }
}

const whatsapp_sendmessage = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;

        let values = '';
        if (data.message_type === 'text') {
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                text: {
                    body: data.message
                }
            }
        }
        else if (data.message_type === 'image') {
            // await UploadAttachmentFile(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                image: {
                    link: data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'document') {
            // await UploadAttachmentFile(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                document: {
                    link: data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }

        await axios({
            url: `${WA_API_URL}/api/v1/messages`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': WA_API_KEY,
            },
            data: JSON.stringify(values),
        })
            .then(async function (result) {
                // console.log(result.data);
                await insertDataMessage(data)
                response.ok(res, result.data);
            })
            .catch(function (error) {
                console.log(error);
                logger('omnichannel/whatsapp', error);
                res.status(500).end();
            });

    }
    catch (error) {
        console.log(error);
        logger('omnichannel/whatsapp', error);
        res.status(500).end();
    }
}

// function non http
const whatsapp_send_messages = async function (data) {
    try {
        const message_id = random_string(20);
        data.message_id = message_id;
        let values = '';
        if (data.message_type === 'text') {
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                text: {
                    body: data.message
                }
            }
        }
        else if (data.message_type === 'image') {
            await UploadAttachmentFile(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                image: {
                    link: data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'document') {
            await UploadAttachmentFile(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                document: {
                    link: data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }

        await axios({
            url: `${WA_API_URL}/api/v1/messages`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': WA_API_KEY,
            },
            data: JSON.stringify(values),
        })
            .then(async function (result) {
                console.log(result.data);
                await insertDataMessage(data)
            })
            .catch(function (error) {
                console.log(error);
                logger('omnichannel/whatsapp', error);
            });

    }
    catch (error) {
        console.log(error);
        logger('omnichannel/whatsapp', error);
    }
}
const insertDataCustomer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ phone_number: data.sender_phone }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.sender_push_name,
                phone_number: data.sender_phone,
                source: 'Whatsapp',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);

        await insert_channel_customer({ customer_id, value_channel: data.sender_phone, flag_channel: 'Phone' });
    }
    return customer_id;
}

const insertDataMessage = async function (data) {
    // const message_id = random_string(20);
    await knex('chats')
        .insert([{
            chat_id: data.chat_id,
            user_id: data.user_id,
            customer_id: data.customer_id,
            message: data.message,
            message_type: data.message_type,
            message_id: data.message_id,
            name: data.name,
            email: data.email,
            agent_handle: data.agent_handle,
            channel: 'Whatsapp',
            flag_to: 'agent',
            status_chat: 'open',
            flag_end: 'N',
            date_create: knex.fn.now()
        }]);
}

const insertDataAttachment = async function (data) {
    if (data.flag_to === 'customer') {
        let filesize = 0;
        let filename = '';
        if (data.message_type === 'image') {
            filename = data.message_id + '.jpeg';
            filesize = data.message_raw.imageMessage.fileLength;
        }
        else if (data.message_type === 'document') {
            const extension = path.extname(data.message_raw.documentMessage.fileName);
            filename = data.message_id + extension;
            filesize = data.message_raw.documentMessage.fileLength;
        }
        else if (data.message_type === 'video') {
            filename = data.message_id + '.mp4';
            filesize = data.message_raw.videoMessage.fileLength;
        }

        if (filename) {
            await knex('chat_attachments')
                .insert([{
                    chat_id: data.chat_id,
                    message_id: data.message_id,
                    file_origin: WA_API_URL + data.attachment_url,
                    file_name: filename,
                    file_type: data.message_type,
                    file_url: APP_URL + '/' + data.url,
                    file_size: filesize,
                }]);
        }
    }
    else {
        await knex('chat_attachments')
            .insert([{
                chat_id: data.chat_id,
                message_id: data.message_id,
                file_origin: data.file_origin,
                file_name: data.file_name,
                file_type: data.message_type,
                file_url: data.file_url,
                file_size: data.file_size,
            }]);
    }

}

const UploadAttachmentFile = async function (data) {
    const { chat_id, channel, message_id, message_type, attachment } = data;
    //upload file attachment
    if (attachment) {
        for (let i = 0; i < attachment.length; i++) {
            let value = { chat_id, channel, message_id, message_type, attachment: attachment[i].attachment, file_origin: attachment[i].file_origin, file_name: attachment[i].file_name, file_size: attachment[i].file_size, file_url: attachment[i].file_url }
            await UploadAttachment(value);
            await insertDataAttachment(value);
        }
    }
}

module.exports = {
    whatsapp_webhook,
    whatsapp_sendmessage,
    whatsapp_send_messages,
}