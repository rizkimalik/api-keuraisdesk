const knex = require('../../config/db_connect');
const axios = require('axios');
const FormData = require('form-data');
const date = require('date-and-time');
const { response, logger } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');

const twitter_get_token = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const check = await knex('sosmed_channels').count('page_id as jml').where({ page_id: data.user_id, channel: 'Twitter' }).first();

        if (check.jml > 0) {
            data.result = 'Update Success.'
            await knex('sosmed_channels')
                .where({ page_id: data.user_id, channel: 'Twitter' })
                .update({
                    page_name: data.screen_name,
                    page_category: '',
                    channel: 'Twitter',
                    account_id: '',
                    token: data.oauth_token,
                    token_secret: data.oauth_token_secret,
                    user_secret: '',
                    updated_at: knex.fn.now(),
                })
        }
        else {
            data.result = 'Insert Success.'
            await knex('sosmed_channels')
                .insert({
                    page_id: data.user_id,
                    page_name: data.screen_name,
                    page_category: '',
                    channel: 'Twitter',
                    account_id: '',
                    token: data.oauth_token,
                    token_secret: data.oauth_token_secret,
                    user_secret: '',
                    created_at: knex.fn.now(),
                })
        }

        response.ok(res, data);
    }
    catch (error) {
        console.log(error);
        logger('webhooks/twitter_token', error);
    }
}

const twitter_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Twitter' }).first();
        return result;
    }
    catch (error) {
        console.log(error);
        logger('omnichannel/twitter_token', error);
    }
}

const twitter_get_directmessage = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const value = req.body;
        const result = await twitter_directmessage_detail(value);

        if (result) {
            const data = result;
            const now = new Date();
            const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
            const chat = await knex('chats').select('chat_id', 'agent_handle')
                .where({
                    user_id: data.from_detail.screen_name,
                    flag_to: 'customer',
                    flag_end: 'N',
                    channel: 'Twitter_DM',
                }).first();

            data.channel = 'Twitter_DM';
            data.customer_id = await insertDataCustomer(data);
            data.chat_id = chat ? chat.chat_id : generate_chatid;
            data.message_type = attachment ? attachment.media.type : 'text';

            await knex('chats')
                .insert([{
                    chat_id: data.chat_id,
                    user_id: data.from_detail.screen_name,
                    message: data.message_data.text,
                    message_type: data.message_type,
                    name: data.from_detail.name,
                    email: data.from_detail.screen_name,
                    message_id: data.id,
                    channel: data.channel,
                    customer_id: data.customer_id,
                    flag_to: 'customer',
                    status_chat: 'open',
                    flag_end: 'N',
                    date_create: knex.fn.now(),
                    // agent_handle: chat.agent_handle,
                    // date_create: data.message_timestamp
                }]);
        }
        else {
            console.log(result)
            logger('omnichannel/twitter_get_directmessage', result);
        }

        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('omnichannel/twitter_get_directmessage', error);
    }
}

const twitter_directmessage_detail = async function (value) {
    try {
        const account = await twitter_token_detail({ page_id: value.for_user_id });
        let data = new FormData();
        data.append('oauth_token', account.token);
        data.append('oauth_token_secret', account.token_secret);
        data.append('id', value.direct_message_events[0].id);

        let response = '';
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/twitter/twitter/getdirectmessageshow`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(function (result) {
                response = result.data.data;
            })
            .catch(function (error) {
                console.log(error);
            });

        return response;
    }
    catch (error) {
        console.log(error);
        logger('omnichannel/twitter_directmessage_detail', error);
    }
}

const twitter_get_mention = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');

        console.log(req.body);

        res.end();
    }
    catch (error) {
        console.log(error);
    }
}


const twitter_post_directmessage = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');

        console.log(req.body);

        res.end();
    }
    catch (error) {
        console.log(error);
    }
}

const insertDataCustomer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ account_id: data.from_detail.screen_name }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.from_detail.name,
                account_id: data.from_detail.screen_name,
                source: 'Twitter',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);

        await insert_channel_customer({
            customer_id: customer_id,
            value_channel: data.from_detail.screen_name,
            flag_channel: 'Twitter'
        });
    }
    return customer_id;
}

module.exports = {
    twitter_get_token,
    twitter_get_directmessage,
    twitter_get_mention,
    twitter_post_directmessage
}



