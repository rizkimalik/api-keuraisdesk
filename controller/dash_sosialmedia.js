'use strict';
const knex = require('../config/db_connect');
// const date = require('date-and-time');
const logger = require('../helper/logger');
const response = require('../helper/json_response');

const agent_online = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const users = await knex('users').select('username', 'max_whatsapp', 'max_chat').where({ login: 1, user_level: 'Layer1' }).orderBy('username', 'asc');
        for (let i = 0; i < users.length; i++) {
            let view_chat = `SELECT chat_id,agent_handle FROM chats WHERE flag_to='customer' AND agent_handle='${users[i].username}' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,agent_handle`;
            let total = await knex.raw(`SELECT COUNT(chat_id) as total_handle FROM (${view_chat}) as view_chat`);
            users[i].total_handle = total[0][0].total_handle;
        }
        response.ok(res, users);
    }
    catch (error) {
        console.log(error);
        logger('dash/agent_online', error);
        res.status(500).end();
    }
}

const total_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const channel = await knex('channels').select('channel').where('active',1).groupBy('channel');
        for (let i = 0; i < channel.length; i++) {
            let view_chat = `SELECT chat_id,agent_handle,channel FROM chats WHERE flag_to='customer' and channel='${channel[i].channel}' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,agent_handle,channel`;
            let total_channel = await knex.raw(`SELECT COUNT(chat_id) as total FROM (${view_chat}) as view_chat`);
            channel[i].total = total_channel[0][0].total;
        }
        response.ok(res, channel);
    }
    catch (error) {
        console.log(error);
        logger('todolist/total_channel', error);
        res.status(500).end();
    }
}

const grafik_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const channel = await knex('channels').select('channel').where('active',1).groupBy('channel');
        for (let i = 0; i < channel.length; i++) {
            let view_chat = `SELECT chat_id,agent_handle,channel FROM chats WHERE flag_to='customer' and channel='${channel[i].channel}' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,agent_handle,channel`;
            let total_channel = await knex.raw(`SELECT COUNT(chat_id) as total FROM (${view_chat}) as view_chat`);
            channel[i].total = total_channel[0][0].total;
        }
        response.ok(res, channel);
    }
    catch (error) {
        console.log(error);
        logger('todolist/total_channel', error);
        res.status(500).end();
    }
}

const top10_chat = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const data = await knex.raw(`SELECT * FROM chats WHERE flag_to='customer' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) ORDER BY id DESC LIMIT 10`);
        response.ok(res, data[0]);
    }
    catch (error) {
        console.log(error);
        logger('todolist/top10_chat', error);
        res.status(500).end();
    }
}

module.exports = {
    agent_online,
    total_channel,
    grafik_channel,
    top10_chat,
}