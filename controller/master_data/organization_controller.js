const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;

        let result;
        if (data == 'active') {
            result = await knex('organizations').where({ active: '1' }).orderBy('id', 'asc');
        }
        else {
            result = await knex('organizations').orderBy('id', 'asc');
        }
        response.ok(res, result)
    } catch (error) {
        console.log(error);
        logger('organization', error);
        res.status(500).end();
    }

}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('organizations').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('organization', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            organization_name,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('organizations')
            .insert([{
                organization_name,
                description,
                active,
                created_by,
                created_at
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        console.log(error);
        logger('organization', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            organization_name,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now()
        } = req.body;

        await knex('organizations')
            .update({
                organization_name,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        console.log(error);
        logger('organization', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('organizations').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('organization', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}