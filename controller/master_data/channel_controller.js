const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;
        
        let result;
        if (data == 'active') {
            result = await knex('channels').select('channel').groupBy('channel').where({ active: '1' });
        }
        else {
            result = await knex('channels').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('channel/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('channels').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('channel/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            channel,
            channel_type,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('channels')
            .insert([{
                channel,
                channel_type,
                description,
                active,
                created_by,
                created_at,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        console.log(error);
        logger('channel/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            channel,
            channel_type,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now(),
        } = req.body;

        await knex('channels')
            .update({
                channel,
                channel_type,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        console.log(error);
        logger('channel/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('channels').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        console.log(error);
        logger('channel/destroy', error);
        res.status(500).end();
    }
}

const channel_type = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const channel_type = await knex('channels').select('channel_type').groupBy('channel_type').where({ active: '1' });
        response.ok(res, channel_type);
    }
    catch (error) {
        console.log(error);
        logger('channel/master_channel', error);
        res.status(500).end();
    }
}


module.exports = {
    index,
    show,
    store,
    update,
    destroy,
    channel_type,
}