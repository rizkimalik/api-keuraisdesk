'use strict';
const { whatsapp_send_messages } = require("../omnichannel/whatsapp");

module.exports = function (socket) {
    socket.on('send-message-whatsapp', async (data) => {
        await whatsapp_send_messages(data);
    });
    
    socket.on('return-message-whatsapp', (data) => {
        socket.to(data.agent_handle).emit('return-message-whatsapp', data); //notused
    });

}